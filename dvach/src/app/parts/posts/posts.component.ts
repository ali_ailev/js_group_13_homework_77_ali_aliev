import { Component, OnInit } from '@angular/core';
import { PostService } from '../../services/post.service';
import { Post } from '../../models/post.model';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.sass']
})
export class PostsComponent implements OnInit {
  posts: Post[] = [];

  constructor(private postsService: PostService) { }

  ngOnInit(): void {
    this.postsService.getPost().subscribe(post => {
      this.posts = post;
    });
  }
}

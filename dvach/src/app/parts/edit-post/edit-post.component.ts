import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PostService } from '../../services/post.service';
import { Router } from '@angular/router';
import { PostData } from '../../models/post.model';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.sass']
})
export class EditPostComponent implements OnInit {
  @ViewChild('f') form!: NgForm;

  constructor(
    private postService: PostService,
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

  onSubmit() {
    const postData: PostData = this.form.value;
    this.postService.createPost(postData).subscribe(() => {
      void this.router.navigate(['/']);
    })
    // window.location.reload();
  }

}



export class Post {
  constructor(
    public name: string,
    public message: string,
    public image: string,
    public id: string,
  ) {}
}

export interface PostData {
  name: string;
  message: string;
  image: File | null;
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Post, PostData } from '../models/post.model';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) {
  }

  getPost() {
    return this.http.get<Post[]>(environment.apiUrl + '/posts').pipe(
      map(response => {
        return response.map(postData => {
          return new Post(
            postData.name,
            postData.message,
            postData.image,
            postData.id,
          );
        });
      })
    )
  }

  createPost(postData: PostData) {
    const formData = new FormData();
    formData.append('name',postData.name);
    formData.append('message',postData.message);
    if(postData.image) {
      formData.append('image',postData.image);
    }
    return this.http.post(environment.apiUrl + '/posts', formData);
  }
}

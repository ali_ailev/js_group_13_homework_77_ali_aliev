const fs = require('fs').promises;
const {nanoid} = require("nanoid");

const filename = './file.json';
let posts = [];

module.exports = {
    async init() {
        try {
            const fileContents = await fs.readFile(filename);
            posts = JSON.parse(fileContents.toString());
        } catch (e) {
            posts = [];
        }
    },
    getItems() {
        return posts;
    },
    getItem(id) {
        return posts.find(p => p.id === id);
    },
    addItem(post) {
        post.id = nanoid();
        posts.push(post);
        return this.save();
    },
    save() {
        return fs.writeFile(filename, JSON.stringify(posts, null, 2));
    }
};
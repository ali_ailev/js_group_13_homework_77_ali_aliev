const express = require('express');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('../consfig');
const DB = require('../fileDB');
const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', (req,res) => {
    const posts = DB.getItems();
    return res.send(posts);
});

router.get('/:id', (req,res) => {
    const post = DB.getItem(req.params.id);
    if(!post) {
        return res.status(404).send({message: "not found"});
    }
    return res.send(post);
});

router.post('/', upload.single('name'), async (req,res) =>{
    try {
        if (!req.body.message) {
            return res.status(400).send({message: 'Message is required'});
        }

        const post = {
            name: req.body.name,
            message: req.body.message,
        };

        if (req.file) {
            post.image = req.file.filename;
        }

        await DB.addItem(post);

        return res.send({message: 'Created new product', id: post.id});
    } catch (e) {

    }

});

module.exports = router;
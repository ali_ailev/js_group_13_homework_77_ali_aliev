const express = require('express');
const DB = require('./fileDB');
const cors = require('cors');
const posts = require('./app/anonim');
const app = express();

const port = 8000;

app.use(cors({origin: 'http://localhost:4200'}));
app.use(express.json());
app.use(express.static('public'));
app.use('/posts', posts);

const run = async () => {
    await DB.init();

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
};

run().catch(e => console.error(e));